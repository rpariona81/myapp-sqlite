//window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
/*
var v = new Vue({
    el: '#app',
    data: {
        url: 'http://localhost:3000/',
        //url: this.$route.query,
        addModal: false,
        editModal: false,
        deleteModal: false,
        query: [],
        search: { text: '' },
        emptyResult: false,
        newUser: {
            firstname: '',
            lastname: '',
            gender: '',
            birthday: '',
            email: '',
            contact: '',
            address: ''
        },
        chooseUser: {},
        formValidate: [],
        successMSG: '',

        //pagination
        currentPage: 0,
        rowCountPage: 10,
        totalUsers: 0,
        pageRange: 2
    },
    created() {
        this.showAll();
    },
    methods: {
        showAll() {

            axios.get(this.url + "people/verPeople").then(function (response) {
                if (response.data.query == null) {
                    v.noResult();
                    console.log("Error de GET");
                } else {
                    v.getData(response.data.query);
                }
            })
        },
        searchUser() {
            var formData = v.formData(v.search);
            axios.post(this.url + "user/searchUser", formData).then(function (response) {
                if (response.data.users == null) {
                    v.noResult()
                } else {
                    v.getData(response.data.users);

                }
            })
        },
        addUser() {
            var formData = v.formData(v.newUser);
            console.log(formData);
            axios.post(this.url + "user/addUser", formData).then(function (response) {
                if (response.data.error) {
                    v.formValidate = response.data.msg;
                } else {
                    v.successMSG = response.data.msg;
                    v.clearAll();
                    v.clearMSG();
                }
            })
        },
        updateUser() {
            var formData = v.formData(v.chooseUser); axios.post(this.url + "user/updateUser", formData).then(function (response) {
                if (response.data.error) {
                    v.formValidate = response.data.msg;
                } else {
                    v.successMSG = response.data.success;
                    v.clearAll();
                    v.clearMSG();

                }
            })
        },
        deleteUser() {
            var formData = v.formData(v.chooseUser);
            axios.post(this.url + "user/deleteUser", formData).then(function (response) {
                if (!response.data.error) {
                    v.successMSG = response.data.success;
                    v.clearAll();
                    v.clearMSG();
                }
            })
        },
        formData(obj) {
            var formData = new FormData();
            for (var key in obj) {
                formData.append(key, obj[key]);
            }
            return formData;
        },
        getData(query) {
            v.emptyResult = false; // se vuelve falso si tiene un registro
            v.totalUsers = query.length // obtiene un total de usuarios
            v.query = query.slice(v.currentPage * v.rowCountPage, (v.currentPage * v.rowCountPage) + v.rowCountPage); //slice the result for pagination

            //si el registro está vacío, retrocede una página
            if (v.query.length == 0 && v.currentPage > 0) {
                v.pageUpdate(v.currentPage - 1)
                v.clearAll();
            }
        },

        selectUser(user) {
            v.chooseUser = user;
        },
        clearMSG() {
            setTimeout(function () {
                v.successMSG = ''
            }, 3000); // desapareciendo el mensaje exitoso en 2 segundos
        },
        clearAll() {
            v.newUser = {
                firstname: '',
                lastname: '',
                gender: '',
                birthday: '',
                email: '',
                contact: '',
                address: ''
            };
            v.formValidate = false;
            v.addModal = false;
            v.editModal = false;
            v.deleteModal = false;
            v.refresh()

        },
        noResult() {

            v.emptyResult = true;  // se convierte en verdadero si el registro está vacío, imprime 'No Record Found'
            v.query = null
            v.totalUsers = 0 // eliminar la página actual si está vacía

        },

        pickGender(gender) {
            return v.newUser.gender = gender // agregar nuevo usuario con la selección de género
        },
        changeGender(gender) {
            return v.chooseUser.gender = gender // actualizar el género
        },
        imgGender(value) {
            return v.url + 'img/gender_' + value + '.png' // para el signo de género de imagen en la tabla
        },
        pageUpdate(pageNumber) {
            v.currentPage = pageNumber; // recibir el número de la página actual provino de la plantilla de paginación
            v.refresh()
        },
        refresh() {
            v.search.text ? v.searchUser() : v.showAll(); // para prevenir
        }
    }
})
*/

Vue.component('data-table', {
    template: '<table></table>',
    props: ['users'],
    data() {
      return {
        headers: [
          { title: 'ID' },
          { title: 'firstname', class: 'some-special-class' },
          { title: 'lastname' },
          { title: 'gender' },
          { title: 'email' },
          { title: 'contact' }
        ],
        rows: [] ,
        dtHandle: null
      }
    },
    watch: {
      users(val, oldVal) {
        let vm = this;
        vm.rows = [];
        // You should _probably_ check that this is changed data... but we'll skip that for this example.
        val.forEach(function (item) {
          // Fish out the specific column data for each item in your data set and push it to the appropriate place.
          // Basically we're just building a multi-dimensional array here. If the data is _already_ in the right format you could
          // skip this loop...
          let row = [];
  
          row.push(item.id);
          row.push(item.firstname);
          row.push(item.lastname);
          row.push(item.gender);
          row.push('<a href="mailto://' + item.email + '">' + item.email + '</a>');
          row.push('<a href="http://' + item.contact + '" target="_blank">' + item.contact + '</a>');
  
          vm.rows.push(row);
        });
  
        // Here's the magic to keeping the DataTable in sync.
        // It must be cleared, new rows added, then redrawn!
        vm.dtHandle.clear();
        vm.dtHandle.rows.add(vm.rows);
        vm.dtHandle.draw();
      }
    },
    mounted() {
      let vm = this;
      // Instantiate the datatable and store the reference to the instance in our dtHandle element.
      vm.dtHandle = $(this.$el).DataTable({
        // Specify whatever options you want, at a minimum these:
        columns: vm.headers,
        data: vm.rows,
        searching: false,
        paging: false,
        info: false
      });
    }  
  });
  
new Vue({
    el: '#tabledemo',
    data: {
      users: [],
      search: ''
    },
    computed: {
      filteredUsers: function () {
        let self = this
        let search = self.search.toLowerCase()
        console.log(this.users);

        return this.users.filter(function (user) {
          return 	user.firstname.toLowerCase().indexOf(search) !== -1 ||
            user.lastname.toLowerCase().indexOf(search) !== -1 ||
            user.gender.indexOf(search) !== -1 ||
            user.email.toLowerCase().indexOf(search) !== -1 ||
            user.contact.toLowerCase().indexOf(search) !== -1
        })
      }
    },
    mounted() {
      let vm = this;
      $.ajax({
        url: 'http://localhost:3000/people/verpeople',
        //url: 'https://jsonplaceholder.typicode.com/users',
        success(res) {
          vm.users = JSON.parse(res);
        }
      });
    },
    methods:{
        filteredUsers: function () {
            let self = this
            let search = self.search.toLowerCase()
            return self.users.filter(function (user) {
              return 	user.firstname.toLowerCase().indexOf(search) !== -1 ||
                user.lastname.toLowerCase().indexOf(search) !== -1 ||
                user.gender.indexOf(search) !== -1 ||
                user.email.toLowerCase().indexOf(search) !== -1 ||
                user.contact.toLowerCase().indexOf(search) !== -1
            })
          }
    }
  });

/*
Vue.component('data-table', {
    template: '<table></table>',
    props: ['users'],
    data() {
      return {
        headers: [
          { title: 'ID' },
          { title: 'Username', class: 'some-special-class' },
          { title: 'Real Name' },
          { title: 'Phone' },
          { title: 'Email' },
          { title: 'Website' }
        ],
        rows: [] ,
        dtHandle: null
      }
    },
    watch: {
      users(val, oldVal) {
        let vm = this;
        vm.rows = [];
        // You should _probably_ check that this is changed data... but we'll skip that for this example.
        val.forEach(function (item) {
          // Fish out the specific column data for each item in your data set and push it to the appropriate place.
          // Basically we're just building a multi-dimensional array here. If the data is _already_ in the right format you could
          // skip this loop...
          let row = [];
  
          row.push(item.id);
          row.push(item.username);
          row.push(item.name);
          row.push(item.phone);
          row.push('<a href="mailto://' + item.email + '">' + item.email + '</a>');
          row.push('<a href="http://' + item.website + '" target="_blank">' + item.website + '</a>');
  
          vm.rows.push(row);
        });
  
        // Here's the magic to keeping the DataTable in sync.
        // It must be cleared, new rows added, then redrawn!
        vm.dtHandle.clear();
        vm.dtHandle.rows.add(vm.rows);
        vm.dtHandle.draw();
      }
    },
    mounted() {
      let vm = this;
      // Instantiate the datatable and store the reference to the instance in our dtHandle element.
      vm.dtHandle = $(this.$el).DataTable({
        // Specify whatever options you want, at a minimum these:
        columns: vm.headers,
        data: vm.rows,
        searching: false,
        paging: false,
        info: false
      });
    }  
  });
  
  new Vue({
    el: '#tabledemo',
    data: {
      users: [],
      search: ''
    },
    computed: {
      filteredUsers: function () {
        let self = this
        let search = self.search.toLowerCase()
        return self.users.filter(function (user) {
          return 	user.username.toLowerCase().indexOf(search) !== -1 ||
            user.name.toLowerCase().indexOf(search) !== -1 ||
            user.phone.indexOf(search) !== -1 ||
            user.email.toLowerCase().indexOf(search) !== -1 ||
            user.website.toLowerCase().indexOf(search) !== -1
        })
      }
    },
    mounted() {
      let vm = this;
      $.ajax({
        url: 'https://jsonplaceholder.typicode.com/users',
        success(res) {
          vm.users = res;
        }
      });
    }
  });
*/
